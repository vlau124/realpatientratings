<html>

<head>
	<link rel="stylesheet" href="main.css" type="text/css" media="screen" />
  	<!-- jquery -->
	<script src="https://ajax.googleapis.com/ajax/libs/jquery/1.12.0/jquery.min.js"></script>
</head>



<body>
	<div align="center">
    <h2>Post Your Tweets here </h2>

    <form id="main-contact-form">
      <div class="form-group">
        <!-- do not delete label-->
        <label for="message" class="h4 ">Message</label>
        <br>
        <textarea id="message" required="required" class="form-control status-box" rows="8"
         placeholder="Message">
        </textarea>
        <br>
        <p> Number of characters left </p>
        <p class="counter">140</p>
      </div>
      
      <div class="form-group">
        <input type="submit" id="submit" class="btn btn-submit button1" value="Tweet">
      </div>
    </form>

    <script>
    $(document).ready(function(){
        $('#submit').click(function(){
          var message = $("#message").val();
          $.ajax({
              type: "POST",
              url:"click.php",
              data: {message},
              success: function(){
                  alert('Tweeted!');
              }
          }); 
        });
        
    });
    </script>
    
    <h2>Display Tweets here</h2>
    
    <ul class="posts">
    </ul>
    <script src="app.js"></script>

  </div>

	
</body>

</html>