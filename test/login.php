<?php
session_start();
require_once('twitteroauth/twitteroauth.php');
include('config.php');

if(isset($_SESSION['name']) && isset($_SESSION['twitter_id'])) //check whether user already logged in with twitter
{
	echo "Name :".$_SESSION['name']."<br>";
	echo "Twitter ID :".$_SESSION['twitter_id']."<br>";
	echo "Image :<img src='".$_SESSION['image']."'/><br>";
	echo "<br/><a href='logout.php'>Logout</a>";
}
else // Not logged in
{
	$connection = new TwitterOAuth($CONSUMER_KEY, $CONSUMER_SECRET);
	$request_token = $connection->getRequestToken($OAUTH_CALLBACK); //get Request Token
  
	if(	$request_token)
	{
		$token = $request_token['oauth_token'];
		$_SESSION['request_token'] = $token;
		$_SESSION['request_token_secret'] = $request_token['oauth_token_secret'];
    
		//An sql database and json file to store information would be better
    //was not aware that opening a new page will create new tokens therefore this doesnt work.
    //file_put_contents('user_token.txt', serialize($_REQUEST['config']));
    //file_put_contents('user_token_secrete.txt', serialize($_REQUEST['config']));
    
    /*
    $myfile1 = fopen("user_token.txt", "w") or die("Unable to open file!");
    $txt = $_SESSION['request_token'];
    fwrite($myfile1, $txt);
    fclose($myfile1);
    
    $myfile2 = fopen("user_token_secrete.txt", "w") or die("Unable to open file!");
    $txt = $_SESSION['request_token_secret'];
    fwrite($myfile2, $txt);
    fclose($myfile2);
    */

		switch ($connection->http_code) 
		{
			case 200:
				$url = $connection->getAuthorizeURL($token);
				//redirect to Twitter .
		    	header('Location: ' . $url); 
			    break;
			default:
			    echo "Coonection with twitter Failed";
		    	break;
		}
	}
	else //error receiving request token
	{
		echo "Error Receiving Request Token";
	}
	
}



?>